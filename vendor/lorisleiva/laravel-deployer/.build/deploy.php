<?php

namespace Deployer;

require 'recipe/laravel-deployer.php';

/*
 * Includes
 */

/*
 * Options
 */

set('strategy', 'basic');
set('application', 'Laravel');
set('repository', 'https://agumaxwellforcode@gitlab.com/agumaxwellforcode/gladpay-bvn-verify.git');

/*
 * Hosts and localhost
 */

host('3.137.113.175')
    ->set('deploy_path', '/var/www/ec2-3-137-113-175.us-east-2.compute.amazonaws.com')
    ->user('ubuntu');

/*
 * Strategies
 */

/*
 * Hooks
 */

after('hook:build', 'npm:install');
after('hook:build', 'npm:production');
after('hook:ready', 'artisan:storage:link');
after('hook:ready', 'artisan:view:clear');
after('hook:ready', 'artisan:config:cache');
after('hook:ready', 'artisan:migrate');