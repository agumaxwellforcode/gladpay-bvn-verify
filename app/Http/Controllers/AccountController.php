<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Response;


class AccountController extends Controller
{
    public function getBanks()
    {
        $response = Http::withHeaders([
            'key' => '123456789',
            'mid' => 'GP0000001'
        ])->put('https://demo.api.glade.ng/resources/',[
            'inquire'=> 'banks'
        ]);
        $response->json();
        // dd($response);
        return  $response->body();
    }

    public function verifyAccount(Request $request)
    {
        $validatedData = $request->validate([
            'bank' => 'required',
            'accountNumber' => 'required|max:10|min:10:numeric',
        ]);

        $response = Http::withHeaders([
            'key' => '123456789',
            'mid' => 'GP0000001'
        ])->put('https://demo.api.glade.ng/', [
            'inquire'=> 'accountname',
            'accountnumber' => $request->accountNumber,
            'bankcode' => $request->bank,
        ]);
        // dd(response::json($response));
        return response::json($response);
    }

    public function verifyBvn(Request $request)
    {
        $validatedData = $request->validate([
            'bvn' => 'required|max:11|min:11:numeric',
        ]);

        $response = Http::withHeaders([
            'key' => '123456789',
            'mid' => 'GP0000001'
        ])->put('https://demo.api.glade.ng/', [
            'inquire'=> 'bvn',
            'bvn' => $request->bvn,
        ]);
        // dd(response::json($response));
        return response::json($response);
    }
   
}
