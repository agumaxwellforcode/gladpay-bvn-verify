<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Gladepay-Verify</title>
        <link rel="icon" href="../img/logo.png">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        {{-- <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet"> --}}
        
       
    
        <style>
            body {
                font-family: 'Nunito';
                height: 100%;
                width: 100%;
            }
            .btn-success{
               background-color: #00afef !important;
               border-color: #00afef !important;
            }
            .btn-outline-success{
               color: #00afef !important;
               border-color: #00afef !important;
            }
        </style>
         <style>
            .animated {
               /* background-image: url(/css/images/logo.png); */
               background-repeat: no-repeat;
               background-position: left top;
               -webkit-animation-duration: 10s;
               animation-duration: 5s;
               -webkit-animation-fill-mode: both;
               animation-fill-mode: both;
            }
            
            @-webkit-keyframes fadeIn {
               0% {opacity: 0;}
               100% {opacity: 1;}
            }
            
            @keyframes fadeIn {
               0% {opacity: 0;}
               100% {opacity: 1;}
            }
            
            .fadeIn {
               -webkit-animation-name: fadeIn;
               animation-name: fadeIn;
            }
         </style>
    </head>
    <body class="">
        <div id="app">
           
          
            <home-component/>
        </div>
      

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
 <!-- Latest compiled and minified JavaScript -->
 <script src="{{asset('js/app.js')}}"></script>
   
</body>
</html>
